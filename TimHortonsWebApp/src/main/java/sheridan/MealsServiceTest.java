package sheridan;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testDrinksRegular() {
		MealsService service = new MealsService();
		List<String> list = service.getAvailableMealTypes(MealType.DRINKS);	
		assertTrue("List is not empty",!list.isEmpty());
	}
	@Test
	public void testDrinksException() {
		MealsService service = new MealsService();
		List<String> list = service.getAvailableMealTypes(null);	
		assertTrue("Correct output",list.get(0).equals("No Brand Available"));
	}
	@Test
	public void testDrinksBoundaryIn() {
		MealsService service = new MealsService();
		List<String> list = service.getAvailableMealTypes(MealType.DRINKS);	
		assertTrue("List is greater than 3",list.size()>3);
	}
	@Test
	public void testDrinksBoundaryOut() {
		MealsService service = new MealsService();
		List<String> list = service.getAvailableMealTypes(null);	
		assertFalse("List is not greater than 1",list.size()>1);
	}

}
